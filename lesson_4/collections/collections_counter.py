import collections

d = collections.Counter('abcsssddd')
print(d)


def counter(s):
    d = {}
    for i in s:
        if i not in d:
            d[i] = 0
        d[i] += 1
    return d


print(counter('abcsssddd'))


