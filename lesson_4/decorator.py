# обернули ф-ю в другую ф-ю

import time
import functools


def timer(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        start_time = time.clock()
        # подсчитали результат
        result = fn(*args, **kwargs)
        print(f'{fn} complete in {time.clock() - start_time} seconds')

        # вернули результат
        return result

    # вернули функцию
    return wrapper


@timer
def pow(a, b):
    return a ** b


@timer
def sqrt(a, b):
    return a ** (1 / b)


# test = pow(10, 2000000)


def auth(fn):
    def wrap(fn):
        def wrapper(request, *args, **kwargs):
            if hasattr(request, 'auth'):
                result = fn(request, *args, **kwargs)
                return result
            raise NotAuthorized

        return wrapper

    return wrap


'''
@role('owner')
def edit_page(request, page_id):
    if request.role == 'admin':
        pass
    pass
'''