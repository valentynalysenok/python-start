# good practice
def side(a, b):
    return {**a, 0: b}


# bad practice - return the same id of var
def side2(a, b):
    return a.update({0: b})


