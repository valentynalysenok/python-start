import functools


def msg(*args):
    print(*args)


hello_msg = functools.partial(msg, 'Hello')
hello_msg() # Hello


def calc(q, p):
    return q * p


calc_test = functools.partial(calc, p=10)
print(calc_test(q=10))

calc_test2 = functools.partial(calc, q=20)
print(calc_test2(p=20))
