import math


def input_number():
    num = float(input("Please input number: "))
    return num


def input_multiple_numbers(i):
    arr = []
    while i > 0:
        num = float(input("Please input number: "))
        i -= 1
        arr.append(num)
    return arr


# 1_сумма чисел
def get_sum_numbers(arr):
    summary = 0
    for num in arr:
        summary += num
    return f"Sum of {len(arr)} numbers is {summary}"


# 2_квадрат и куб числа
def get_pow(num):
    pow_2 = num ** 2
    pow_3 = num ** 3
    return f"The square and cube of number are {pow_2} and {pow_3}"


# 3_сумма форматированных чисел
def get_sum_arithmetical(a, b, c):
    a *= 2
    b /= 3
    c **= 2
    return round(sum([a, b, c]), 2)


# 4_разность удвоенной суммы первого и третьего чисел и утроенного второго числа
def get_sum_arithmetical_2(a, b, c):
    s = (a + c) * 2
    b *= 3
    r = s - b
    return round(r, 2)


# 4_среднее чисел
def get_average(*a):
    summary = 0
    for num in a:
        summary += num
        avg = round(summary / len(a), 2)
    return f"Average of {len(a)} numbers is {avg}"


# 5_расчет S и p квадрата
def find_square_area(square_side):
    area = round(square_side ** 2, 2)
    return f"Area of square is {area}"


def find_square_perimeter(square_side):
    perimeter = round(square_side * 4, 2)
    return f"Perimeter of square is {perimeter}"


# 6_покупки
def ask_price():
    p_candies = float(input("Please input price/kg for candies: "))
    p_cookies = float(input("Please input price/kg for cookies: "))
    return p_candies, p_cookies


def get_sum_user_purchases(candies_kg, cookies_kg, qty_purchases=1):
    p_candies, p_cookies = ask_price()
    sum_purchases = round(((candies_kg * p_candies + cookies_kg * p_cookies) * qty_purchases), 2)
    return f"The sum of user purchases: {sum_purchases}"


# 7_скорость в м/c
def find_speed():
    minutes = int(input("Please input time in minutes: "))
    distance_km = int(input("Please enter distance in kilometers: "))
    result_ms = round((distance_km * 1000) / (minutes * 60), 2)
    return f"Speed in m/s is: {result_ms}"


# 8_расчет S, p, h прямоугольного треугольника
def find_right_trg_area(trg_leg_1, trg_leg_2):
    square = round((0.5 * trg_leg_1 * trg_leg_2), 2)
    return f"Area of right triangle is {square}"


def find_right_trg_hypotenuse_perimeter(trg_leg_1, trg_leg_2):
    hypotenuse = round(math.sqrt(trg_leg_1 ** 2 + trg_leg_2 ** 2), 2)
    perimeter = trg_leg_1 + trg_leg_2 + hypotenuse
    return f"Hypotenuse of right triangle is {hypotenuse}, perimeter is {perimeter}"


# 9_конвертор Цельсий/Фаренгейт
def temp_converter(celsius_degree):
    fahrenheit_degree = round(((celsius_degree * 9 / 5) + 32), 2)
    return f"{celsius_degree} Celsius is {fahrenheit_degree} Fahrenheit degrees"
