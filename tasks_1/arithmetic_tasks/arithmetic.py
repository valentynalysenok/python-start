import tasks_1.arithmetic_tasks.arithmetic_func as f

print("Пользователь вводит два числа. Найдите сумму и произведение данных чисел:")
arr_1 = f.input_multiple_numbers(2)
print(f.get_sum_numbers(arr_1))

print("Пользователь вводит число. Выведите на экран квадрат этого числа, куб этого числа:")
num = f.input_number()
print(f.get_pow(num))

print("Пользователь вводит три числа. Увеличьте первое число в два раза, второе числа уменьшите на 3, "
      "третье число возведите в квадрат и затем найдите сумму новых трех чисел:")
arr_2 = f.input_multiple_numbers(3)
print(f.get_sum_arithmetical(*arr_2))

print("Пользователь вводит три числа. Найдите среднее арифметическое этих чисел, "
      "а также разность удвоенной суммы первого и третьего чисел и утроенного второго числа:")
arr_3 = f.input_multiple_numbers(3)
print(f.get_average(*arr_3))
print(f.get_sum_arithmetical_2(*arr_3))

print("Пользователь вводит сторону квадрата. Найдите периметр и площадь квадрата:")
square_side = f.input_number()
print(f.find_square_perimeter(square_side))
print(f.find_square_area(square_side))

print("Пользователь вводит цены 1 кг конфет и 1 кг печенья. Найдите стоимость: "
      "а) одной покупки из 300 г конфет и 400 г печенья; б) трех покупок, каждая из 2 кг печенья и 1 кг 800 г конфет.")
print(f.get_sum_user_purchases(0.3, 0.4))
print(f.get_sum_user_purchases(1.8, 2, 3))

print("Пользователь вводит время в минутах и расстояние в километрах. Найдите скорость в м/c:")
print(f.find_speed())

print("Даны катеты прямоугольного треугольника. Найдите площадь, периметр и гипотенузу треугольника.ренгейта:")
print(f.find_right_trg_area(10, 20))
print(f.find_right_trg_hypotenuse_perimeter(10, 30))

print("Дано значение температуры в градусах Цельсия. Вывести температуру в градусах Фаренгейта:")
print(f.temp_converter(32))


