import tasks_1.string_tasks.strings_func as f

print('1_Дана строка. Определить является ли количество символов четным:')
print(f.check_even_length('Nisl scelerisque justo per hac cras purus lectus maecenas litora facilisi potenti.'))

# 2_Сформировать строку из 10 символов. На четных позициях должны находится четные цифры, на нечетных позициях - буквы.

print('3_Дана строка. Показать порядковый номер символов, совпадающих с последним символом строки:')
print(f.find_last_symbol_repetition('Nisl scelerisque justo per hac cras purus lectus maecenas litora facilisi potenti'))

print('4_Дана строка. Определите, какой символ в ней встречается раньше: \'x\' или \'w\'.'
      'Если какого-то из символов нет, вывести сообщение об этом:')
print(f.find_symbol("xow"))

print('5_Дана строка. Если ее длина больше 10, то оставить в строке только первые 6 символов,'
      'иначе дополнить строку символами \'o\' до длины 12:')
print(f.string_add_symbols('again'))

print('6_Дана строка. Посчитать количество слов строке:')
print(f.count_words('Nisl scelerisque justo per hac cras purus lectus maecenas litora facilisi potenti.'))

print('7_Дана строка. Заменить каждый четный символ или на \'a\', если символ не равен \'a\' или \'b\', '\
      'или на \'c\' в противном случае:')
print(f.replace_symbol('dmdmdm'))

print('8_Дана строка. Если она начинается на \'abc\', то заменить их на \'www\', иначе добавить в конец строки \'zzz\':')
print(f.replace_symbols('abcabcabc'))

print('9_Дано две строки. Сравнить совпадают ли в них первые и последние 4 символа:')
print(f.check_start_end_symbols('ellaemma', 'ellaandemma'))

print('10_Дана строка. Вывести средний символ в нижнем регистре:')
print(f.get_avg_symbol('ELKMA'))