# 1
def check_even_length(string):
    if len(string) % 2 == 0:
        return True
    else:
        return False


# 2
def check_str(s):
    for chr_idx, chr in enumerate(s):
        if (chr_idx % 2 == 0 and isinstance(chr, int)) and (chr_idx % 2 != 0 and isinstance(chr, str)):
            return True
        else:
            return False


print(check_str('0a2b4c6d8e'))


# 3
def find_last_symbol_repetition(string):
    '''arr = []
    letter = string[len(string) - 1]
    for i in range(len(string)):
        if string[i] == letter:
            arr.append(i + 1)
    return arr'''
    arr = []
    letter = string[len(string) - 1]
    for chr_idx, chr in enumerate(string):
        if letter == chr:
            arr.append(chr_idx)
    return arr


# 4
def find_symbol(string):
    if string.count('x') > 0 and string.count('w') > 0:
        if string.index("x") < string.index("w"):
            print("x earlier than w")
        else:
            print("w earlier than x")
    else:
        print("There are no x or w symbols in a string")


# 5
def string_add_symbols(string):
    new = ''
    if len(string) > 10:
        new = string[:6]
    else:
        new = string.ljust(12, 'o')
    return new


# 6
def count_words(string):
    arr = string.split()
    return len(arr)


# 7
def replace_symbol(string):
    arr = list(string)
    for i in range(len(arr)):
        if i % 2 != 0:
            if arr[i] != 'a' or arr[i] != 'b':
                arr[i] = 'a'
            else:
                arr[i] = 'c'
    return ''.join(arr)

# 8
def replace_symbols(string):
    if string.startswith('abc'):
        return string.replace('abc', 'www', 1)
    else:
        return string + 'zzz'


# 9
def check_start_end_symbols(str_1, str_2):
    return str_1[:4] == str_2[:4] and str_1[-4:] == str_2[-4:]


# 10
def get_avg_symbol(string):
    if len(string) % 2 != 0:
        symbol = int(len(string) / 2)
        return string[symbol].lower()
    else:
        return "The string has even numbers of symbols"
