first, *all, last = (1, 123344, 34555, 5555, "last_column")
print(all)

a, b = 10, 20
a, b = b, a

a, *_, b = 1, 2
print(_)  # []


list1 = [1, 2, 3, 4, 5, 6]
list2 = [7, 8, 9, 10]
tuple_test = (*list1, *list2)
print(tuple_test)  # (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
