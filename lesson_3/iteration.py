a = list(range(10))

i = iter(range(10))
print(i)

print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
# print(next(i))  # error => StopIteration


i = iter(enumerate('abcdff'))
print(next(i))  # (0, 'a') <= tuple

for i, item in enumerate([0, 1, 2, 3, 4, 5]):
    print(i, item)

for i, item in enumerate({0, 1, 2, 3, 4, 5}):  # set we couldn't use because every time we had another result
    print(i, item)
