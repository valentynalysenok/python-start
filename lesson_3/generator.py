# iterator
# generator используем только 1 раз


def fib(n, cur=1, prev=0):
    while n:
        cur, prev = cur + prev, cur
        yield cur  # function is still working
        n -= 1
    #raise StopIteration


num = fib(10)
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))
print(next(num))


for i in fib(10):
    print(i)

s = 'emma'
l = list(s)
print(l)
l.reverse()
new_s = ''.join(l)
print(new_s)  # amme
print(''.join(reversed('emma')))  # amme

