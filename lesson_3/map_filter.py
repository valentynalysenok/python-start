def get_sum(n):
    return n + n


result = list(map(get_sum, range(10)))
print(result)  # [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]

result_1 = [i ** 2 for i in range(10)]
print(result_1)

result_2 = list(map(lambda x: x ** 2, range(10)))
print(result_2)