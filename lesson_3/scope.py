a = 10


def test():
    global a
    a = 15  # local variable
    print(a)
    def f():
        global a
        a = 30  # local variable
        print(a)
    return f()

test()

'''def test():  #error
    print(a)
    a = 20
    print(a)'''


if True:     # not a scope as for cycle -> w/o error
    print(a)
    a = 20
    print(a)


# global variable
def test2():
    # print(a) # will be error, we try to catch global variable w/o abr
    global a
    a = 15
    print(a)


test2()


# non-local variable
def fn():
    a = 11
    def f():
        nonlocal a
        print(a)
    return f()

fn()