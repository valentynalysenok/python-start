# 1. Соединить словари

```
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
Expected Result : {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
```

# 2. Написать функцию, которая определяет есть ли ключ в словаре
```
def check_key(d, k):
    pass


d = {1: True, 2: 'abc', 'msg': 'hello'}
```
Expected Result :
```
check_key(d, 'msg')  # True
check_key(d, 'abc')  # False
```

# 3. Составить таблицу умножения до 10 с помощью словаря


# 4. Написать функцию, которая выводит словарь в виде таблицы


# 5. Написать фукцию, которая при объединении двух и более словарей возращает, только те ключи, которые уникальны для каждого.
```
d1 = {1: True, 2: 'abc', 'd': 'hello'}
d2 = {5: True, 2: 'abc', 'msg': 'hello'}
d3 = {1: True, 4: 'abc', 'msg': 'hello'}

def merge(*args):
    pass
```
Expected Result :
```
pass(d1, d2, d3) # {2: 'abc', 'd': 'hello', 5: True, 4: True}
```

# Найти количество комбинаций букв в тексте. Результат представить в виде словаря.
```
s = '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum volutpat velit, sit amet consequat massa convallis in. Mauris sollicitudin fringilla augue et pellentesque. Curabitur at sapien in dolor malesuada luctus. Nullam. '''

def counter(s):
    pass
```
Expected Result :
```
counter(s) # {'lo': 5, 'or': 5, ...}
```

# Написать функцию, которая будет заполнять журнал посещаимости
```
journal = {
    'Petrov': [
        {'date': '31.03.2019', 'is_attended': True}
    ]
}

change_status('Pertov', '31.03.2019')  # изменит is_attended на противоположное.
change_status('Ivanov', '31.03.2019')  # добавит ключ "Ivanov", создаст запись о посещаимости
change_status('Ivanov', '01.04.2019')  # добавит данные о посещаимости
```