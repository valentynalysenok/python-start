# 1_Соединить словари
from collections import defaultdict


def join_dicts(*dict_args):
    result = {}
    for dict in dict_args:
        result.update(dict)
    return result


dic1 = {1: 10, 2: 20}
dic2 = {3: 30, 4: 40}
dic3 = {5: 50, 6: 60}
print(join_dicts(dic1, dic2, dic3))


# 2_Написать функцию, которая определяет есть ли ключ в словаре
def check_key(dict, key_name):
    return key_name in dict


d1 = {1: True, 2: 'abc', 'msg': 'hello'}
print(check_key(d1, 'msg'))  # True
print(check_key(d1, 'abc'))  # False


# 3_Составить таблицу умножения до 10 с помощью словаря
def create_calc_table(dict):
    for key in range(1, len(dict) + 1):
        for value in range(1, len(dict) + 1):
            print(key * value, end='\t')
        print()


d2 = {1: 1, 2: 2, 3: 5}
create_calc_table(d2)


# 4_Написать функцию, которая выводит словарь в виде таблицы
def create_table(dict):
    print('{:<20} {:<15}'.format('Column_1', 'Column_2'))
    for key, value in dict.items():
        print('{:<20} {:<15}'.format(key, value))


d3 = {1: "Test1", 2: "Test2", 3: "Test3"}
create_table(d3)


# 5_Написать фукцию, которая при объединении двух и более словарей возращает, только те ключи,
# которые уникальны для каждого.
def get_unique_keys(*dict_args):
    new_dict = {}
    for dict in dict_args:
        if 'k' not in dict:
            new_dict.update(dict)
    return new_dict


dict1 = {1: True, 2: 'abc', 'd': 'hello'}
dict2 = {5: True, 2: 'abc', 'msg': 'hello'}
dict3 = {1: True, 4: 'abc', 'msg': 'hello'}
print(get_unique_keys(dict1, dict2, dict3))

# 6_Найти количество комбинаций букв в тексте. Результат представить в виде словаря.


# 7_Написать функцию, которая будет заполнять журнал посещаимости
