print('1_Создать функцию, которая будет добавлять книги в некую упорядоченую последовательность.')


def add_book(list_of_books, book_name):
    return list_of_books.append(book_name)


books = ['Harry Potter and the Philosopher\'s Stone', 'The Hunger Games', 'Winnie-the-Pooh']
add_book(books, 'The Cat in the Hat')
print(books)

print('2_Дан список состоящий чисел. Выяснить сколько раз входит ли некоторое число в список.')


def find_num_in_list(list_of_nums, number):
    count = 0
    for i in list_of_nums:
        if i == number:
            count += 1
    return count


numbers = [1, 23, 23, 23, 45, 7, 89, 1]
num_1 = find_num_in_list(numbers, 23)
num_2 = find_num_in_list(numbers, 100)
print(num_1)
print(num_2)

print('3_Найдите сумму четных чисел списка которые являются числом.')


def get_sum_from_list(list_of_el):
    sum_of_el = 0
    for i in list_of_el:
        if not isinstance(i, str) and i % 2 == 0:
            sum_of_el += i
    return sum_of_el


list_ex = [1, 23, 23, 23, 45, 7, 89, 10, 22, 'me', 'you', 'we']
el = get_sum_from_list(list_ex)
print(el)

print('4_Дан пустой список. Заполнить список путем ввода данных с клавиатуры. Найти наибольший элемент списка.')


def create_list_from_input():
    list_of_el = []
    while True:
        el = input('Input some random value: ')
        if el == 'exit':
            break
        else:
            list_of_el.append(el)
    return list_of_el


print(create_list_from_input())

print('5_Составить список из списков. Написать функцию, которая проверит, '
      'является хоть один из вложенных списков не пустым')


def check_list_not_empty(list_of_el):
    for el in list_of_el:
        if not isinstance(el, list) or check_list_not_empty(el):
            return False
    return True


list_1 = [[1, 0.5], [1, 4], [5, 0.5], [5, 4], [8, 0.5], [8, 4]]
list_2 = [[1, 0.5], [1, 4], [5, 0.5], [5, 4], [8, 0.5], []]
print(check_list_not_empty(list_1))
print(check_list_not_empty(list_2))

print('6_Составить строку из элементов списка.')


def create_str_from_list_el(list_of_el):
    return ''.join(map(str, list_of_el))


list_ex2 = ['me', 'you', 'we', 1]
print(create_str_from_list_el(list_ex2))
