1. Написать "чистую"-функцию, которая в списке из чисел, каждое число умножит на "n"

```
def mul_seq(seq, n):
    pass
    
mul_seq(range(3), 4)
# <generator 0x...> (0, 8, 16)

mul_seq([0, 1, 2], 4)
# [0, 8, 16]

mul_seq((1, 2, 3), 4)
# (4, 8, 16)

```

2. Написать генератор таблицы умножения на "n"
```
def get_mul_table(n):
    pass
    
i = get_mul_table(2)
next(i) # 2
next(i) # 4
next(i) # 8
next(i) # 16
```

3. Написать функцию, которая будет константно определять аргументы к другой функции.
```
def list_important_things(primary, *args):
     print(primary, *args)

def rpart(fn, *args, **kwargs):
    pass
    
list_dispute_args = rpart(list_important_things, 'и животноводство', 'and my axe!')
list_dispute_args(input('dispute args: '))
```

4. Напишите декоратор, который выводит время вызова функции, название, её параметры и 
опционально откладывает выполнение на "n" секунд.
```
@decorator
def my_fn():
    print('"my_fn" executed')
    
@decorator(sleep=1.0)
def my_delayed_fn():
    print('"my_delayed_fn" executed')

my_fn()          # выполняется сразу
my_delayed_fn()  # выполняется через 1 сек.
```