# Написать "чистую"-функцию, которая в списке из чисел, каждое число умножит на "n"
def mul_seq(seq, n):
    new_seq = [i * n for i in seq]
    return new_seq


print(mul_seq(range(3), 4))
print(mul_seq([0, 1, 2], 4))
print(mul_seq((1, 2, 3), 4))


# Написать генератор таблицы умножения на "n"
def get_mul_table(n):
    return iter([i * n for i in range(1, 10)])


i = get_mul_table(2)
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))
print(next(i))