from time import sleep


def delay(seconds=0):
    def delay_decorator(func):
        def inner():
            sleep(seconds)
            return func()
        return inner
    return delay_decorator


@delay(seconds=0)
def my_fn():
    print('\nshow text')


@delay(seconds=1.0)
def my_delayed_fn():
    print('\nshow text after several seconds')


my_fn()
my_delayed_fn()
