from time import sleep


def delay(seconds=0):
    def delay_decorator(func):
        def inner(*args, **kwargs):
            sleep(seconds)
            return func(*args, **kwargs)
        return inner
    return delay_decorator


@delay(seconds=0)
def my_fn(times):
    print('\nshow text' * times)


@delay(seconds=1.0)
def my_delayed_fn(times):
    print('\nshow text after several seconds' * times)


my_fn(1)
my_delayed_fn(3)
