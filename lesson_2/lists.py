import copy
a = list()
b = 1
a.append(b)
a.append(b)
c = [2]
a.append(c)
print(a[1:])
print(a[-1])
c.append(4)
arr = [i for i in range(20)]
print(arr)
list('test')
# li[index]
# append()
# extend()
# insert()
# remove()
# pop()
# len(li)
# li.sort() # li.sort(reverse=True)
# li.copy() # дорогая операция для памяти
# li.reverse() не мутируем изначальный # перевернуть список li[::-1] create new object
# li.clear()  # delete all links

# варианты копирования списка
a = [1, 2, 3]
b = a[:]  # create copy of list
b = copy.deepcopy(a)  # мы копируем объект, обеспечивая себя от ссылочного копирования
