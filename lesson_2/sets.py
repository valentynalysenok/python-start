# only unique elements
# не итерируемый
# cannot add lists (because it mutable and has links)
# cannot add dictionary (because it mutable and has links)

set_test = {1, }
s = set('aaaaa')
print(s)  # {'a'}
s2 = set('wabc')

# sort set
a = list(s2)
a.sort()
print(a)  # ['a', 'b', 'c', 'w']

# s.update(s2)
# s.intersection_update(s2) => пересичение
# s.issubset(s2)
# s.issuperset(s2)
# s.union(s2)
# s.difference(s2)
# s.intersection(s2)
# s.isdisjoint....
# s.symmetric_difference(s2)
# ...

fs = frozenset({1, 2, 3})
print(fs)
