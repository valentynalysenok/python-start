# immutable!!!
# create tuple
t1 = tuple()
t2 = (1, )
t3 = (1, 'abc', 'wer')

t4 = tuple('max')
print(t4)  # ('m', 'a', 'x')

a = 1
b = (2)  # int -> w/o comma
c = (3, )
print(type(b))
