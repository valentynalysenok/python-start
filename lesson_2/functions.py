# def func(a=123, b)  # error
# def func(a, b='default', *args)


def func(a, b='default', *args):
    print(a, b, args)


print(func(1, 2, 3, 4, 5, 6))  # 1 2 (3, 4, 5, 6)


def func2(a, b='default', **kwargs):
    print(a, b, kwargs)


print(func2(1, 2, m=3, z=4))  # 1 2 {'m': 3, 'z': 4}

# one * near var => return list
# two ** near var => return dictionary