# keys are set
# keys can be functions / classes / bools

d = dict()

d2 = {'key': 123}
d2['key'] = 321

d2['key'] = [1, 2, 3]
print(d2['key'][1])  # 2

d3 = dict([['key-1', 2], ['key-2', 3]])
print(d3)  # {'key-1': 2, 'key-2': 3}

l = list(d3)
print(l)  # ['key-1', 'key-2']

l2 = list(d3.values())
print(l2)  # [2, 3]

# d.copy()
# d.clear()
# d.get(key, default)  # default str value => return message if key not exist
# d.items()
# d.keys()
# d.pop(key)   # delete any key
# d.popitem()  # delete last item and return this key/value as tuple
# len()  # quantity of keys (iterable)
# d.update()
# d.setdefault()  # very rare function
# key in d
#

